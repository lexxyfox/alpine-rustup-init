* Designed for use with automated CI pipelines.
* Currently for x86_64 only.
* I'll sign my stuff when I get around to it :v

## How to install:

```bash
echo https://lexxyfox.gitlab.io/alpine-gn >> /etc/apk/repositories
apk add -U --allow-untrusted gn
```

